﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyroXJog
{
    class Globals
    {
        //        public static StorageFile profiili1_file, profiili2_file;
        //        public static float[] profiili1_suht, profiili2_suht;
        public static ObservableCollection<Coordinate> coordinates_1 = new ObservableCollection<Coordinate>();
        public static ObservableCollection<Coordinate> coordinates_2 = new ObservableCollection<Coordinate>();
        public static ObservableCollection<Coordinate> coordinates_1_C = new ObservableCollection<Coordinate>();
        public static ObservableCollection<Coordinate> coordinates_2_C = new ObservableCollection<Coordinate>();
        public static ObservableCollection<Step> step_cnt_1 = new ObservableCollection<Step>();
        public static ObservableCollection<Step> step_cnt_2 = new ObservableCollection<Step>();
        public static double twist_1 = 0, twist_2 = 0, sweep = 0, length_1 = 0, length_2 = 0, panel_length = 0;
        public static bool left = true;
        public static double x_spr, y_spr, x_pitch, y_pitch, speed, cutspeed;
        public static double length_1_C = 0, length_2_C = 0;
        public static string DATA1 = "", DATA2 = "";
    }

    class Coordinate
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Coordinate(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }
    }

    class Step
    {
        public int X_dir { get; set; }
        public int X_step { get; set; }
        public int Y_dir { get; set; }
        public int Y_step { get; set; }

        public Step(int x_dir, int x_step, int y_dir, int y_step)
        {
            this.X_step = x_step;
            this.X_dir = x_dir;
            this.Y_step = y_step;
            this.Y_dir = y_dir;
        }
    }
}
