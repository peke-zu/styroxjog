﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace StyroXJog
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private SerialDevice serialPort = null;
        DataWriter dataWriteObject = null;
        DataReader dataReaderObject = null;
        private ObservableCollection<DeviceInformation> listOfDevices;
        private CancellationTokenSource ReadCancellationTokenSource;
        string arduinoInput = "K";

        public MainPage()
        {
            this.InitializeComponent();
            Loaded += MainPage_Loaded;
            //Disable_buttons();
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            // comPortInput.IsEnabled = false;
            listOfDevices = new ObservableCollection<DeviceInformation>();
            ListAvailablePorts();
            Load_settings();
            Disable_buttons();
        }

        /// <summary>
        /// ListAvailablePorts
        /// - Use SerialDevice.GetDeviceSelector to enumerate all serial devices
        /// - Attaches the DeviceInformation to the ListBox source so that DeviceIds are displayed
        /// </summary>
        private async void ListAvailablePorts()
        {
            try
            {
                string aqs = SerialDevice.GetDeviceSelector();
                var dis = await DeviceInformation.FindAllAsync(aqs);

                var resourceLoader = ResourceLoader.GetForCurrentView();
                status.Text = resourceLoader.GetString("valitseportti");

                for (int i = 0; i < dis.Count; i++)
                {
                    listOfDevices.Add(dis[i]);
                }

                DeviceListSource.Source = listOfDevices;
                //                comPortInput.IsEnabled = true;
                ConnectDevices.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
            }
        }

        private void ConnectDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selection = ConnectDevices.SelectedItems;

            if (selection.Count > 0)
            {
                comPortInput.IsEnabled = true;
                return;
            }
            else comPortInput.IsEnabled = false;

        }

        /// <summary>
        /// comPortInput_Click: Action to take when 'Connect' button is clicked
        /// - Get the selected device index and use Id to create the SerialDevice object
        /// - Configure default settings for the serial port
        /// - Create the ReadCancellationTokenSource token
        /// - Start listening on the serial port input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void comPortInput_Click(object sender, RoutedEventArgs e)
        {
            var selection = ConnectDevices.SelectedItems;

            var resourceLoader = ResourceLoader.GetForCurrentView();

            if (selection.Count <= 0)
            {

                status.Text = resourceLoader.GetString("valitseportti");
                return;
            }

            DeviceInformation entry = (DeviceInformation)selection[0];

            try
            {
                serialPort = await SerialDevice.FromIdAsync(entry.Id);
            }
            catch (Exception)
            {
                await Task.Delay(1000);
                serialPort = await SerialDevice.FromIdAsync(entry.Id);
            }

            try
            {
                if (serialPort == null) return;

                //onko arduino
                //arduino = true;

                // Disable the 'Connect' button 
                comPortInput.IsEnabled = false;
                closeDevice.IsEnabled = true;

                // Configure serial settings
                serialPort.WriteTimeout = TimeSpan.FromMilliseconds(1000);
                serialPort.ReadTimeout = TimeSpan.FromMilliseconds(1000);
                serialPort.BaudRate = 115200;
                //serialPort.BaudRate = 9600;
                serialPort.Parity = SerialParity.None;
                serialPort.StopBits = SerialStopBitCount.One;
                serialPort.DataBits = 8;
                serialPort.Handshake = SerialHandshake.None;

                // Display configured settings
                status.Text = resourceLoader.GetString("porttiasetettu");
                status.Text += serialPort.BaudRate + "-";
                status.Text += serialPort.DataBits + "-";
                status.Text += serialPort.Parity.ToString() + "-";
                status.Text += serialPort.StopBits;

                // Set the RcvdText field to invoke the TextChanged callback
                // The callback launches an async Read task to wait for data
                //                rcvdText.Text = "Waiting for data...";

                // Create cancellation token object to close I/O operations when closing the device
                ReadCancellationTokenSource = new CancellationTokenSource();

                // Enable 'WRITE' button to allow sending data
                //                sendTextButton.IsEnabled = true;

                Enable_buttons();

                Listen();
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
                comPortInput.IsEnabled = true;
                //                sendTextButton.IsEnabled = false;
                closeDevice.IsEnabled = false;
            }
        }

        /// <summary>
        /// - Create a DataReader object
        /// - Create an async task to read from the SerialDevice InputStream
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Listen()
        {
            try
            {
                if (serialPort != null)
                {
                    dataReaderObject = new DataReader(serialPort.InputStream);

                    // keep reading the serial input
                    while (true)
                    {
                        await ReadAsync(ReadCancellationTokenSource.Token);
                    }
                }
            }
            catch (TaskCanceledException tce)
            {
                status.Text = "Reading task was cancelled, closing device and cleaning up";
                CloseDevice();
                Disable_buttons();
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
                Disable_buttons();
                closeDevice.IsEnabled = true; //this must be enabled in this case
            }
            finally
            {
                // Cleanup once complete
                if (dataReaderObject != null)
                {
                    dataReaderObject.DetachStream();
                    dataReaderObject = null;
                }
            }
        }

        /// <summary>
        /// CloseDevice:
        /// - Disposes SerialDevice object
        /// - Clears the enumerated device Id list
        /// </summary>
        private void CloseDevice()
        {
            if (serialPort != null)
            {
                serialPort.Dispose();
            }
            serialPort = null;

            comPortInput.IsEnabled = true;
            //           sendTextButton.IsEnabled = false;
            //           rcvdText.Text = "";
            closeDevice.IsEnabled = false;
            listOfDevices.Clear();

            Disable_buttons();
        }

        /// <summary>
        /// closeDevice_Click: Action to take when 'Disconnect and Refresh List' is clicked on
        /// - Cancel all read operations
        /// - Close and dispose the SerialDevice object
        /// - Enumerate connected devices
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void closeDevice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //status.Text = "";
                await Call_write(69, 0);
                await Call_write(67, 0);
                CancelReadTask();
                //await WriteAsync_Own((byte)67, (byte)(49)); // tell arduino to close, easier to make new connect
                CloseDevice();
                ListAvailablePorts();
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
            }
        }

        /// <summary>
        /// CancelReadTask:
        /// - Uses the ReadCancellationTokenSource to cancel read operations
        /// </summary>
        private void CancelReadTask()
        {
            if (ReadCancellationTokenSource != null)
            {
                if (!ReadCancellationTokenSource.IsCancellationRequested)
                {
                    ReadCancellationTokenSource.Cancel();
                }
            }
        }

        /// <summary>
        /// ReadAsync: Task that waits on data and reads asynchronously from the serial device InputStream
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task ReadAsync(CancellationToken cancellationToken)
        {
            Task<UInt32> loadAsyncTask;

            uint ReadBufferLength = 1;

            // If task cancellation was requested, comply
            cancellationToken.ThrowIfCancellationRequested();

            // Set InputStreamOptions to complete the asynchronous read operation when one or more bytes is available
            dataReaderObject.InputStreamOptions = InputStreamOptions.Partial;
            using (var childCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken))
            {
                // Create a task object to wait for data on the serialPort.InputStream
                loadAsyncTask = dataReaderObject.LoadAsync(ReadBufferLength).AsTask(childCancellationTokenSource.Token);

                // Launch the task and wait
                //UInt32 bytesRead=0;
                UInt32 bytesRead = await loadAsyncTask;
                if (bytesRead > 0)
                {
                    //                    rcvdText.Text = dataReaderObject.ReadString(bytesRead);
                    //                    status.Text = dataReaderObject.ReadString(bytesRead) + " bytes read successfully!";
                    arduinoInput = dataReaderObject.ReadString(bytesRead);
                    //DateTime dateTime = DateTime.Now;
                }
            }
        }

        private async Task WriteAsync_Own(byte cmd, byte to_arduino)
        {
            Task<UInt32> storeAsyncTask;
            dataWriteObject = new DataWriter(serialPort.OutputStream);
            // Load the text from the sendText input text box to the dataWriter object
            //dataWriteObject.WriteByte(byte_character);
            byte[] byte_array = new byte[2];
            byte_array[0] = cmd;
            byte_array[1] = to_arduino;
            dataWriteObject.WriteBytes(byte_array);

            // Launch an async task to complete the write operation
            try
            {
                storeAsyncTask = dataWriteObject.StoreAsync().AsTask();
                try
                {
                    UInt32 bytesWritten = await storeAsyncTask;
                }
                catch (Exception ex)
                {
                    string str = ex.Message;
                    status.Text = ex.Message;
                }
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
            }
            dataWriteObject.DetachStream(); //Ilman tätä ei toimi
            dataWriteObject.Dispose();
        }

        private async void Print_out(int command, int to_arduino, bool x_ax)
        {
            Disable_buttons();

            int movement = (int)(number_value(movement_value.Text.ToString()) + 0.5);
            int steps = 0;

            if (x_ax)
            {
                steps = movement * (int)(0.5 + Globals.x_spr / Globals.x_pitch);
            }
            else
            {
                steps = movement * (int)(0.5 + Globals.y_spr / Globals.y_pitch);
            }

            int l = 0;
            while (l < steps)
            {
                try
                {
                    await Call_write(77, (byte)to_arduino); // 77 M as move
                }
                catch (Exception ex)
                {
                    MessageDialog showDialog1 = new MessageDialog("Stopped?");
                    var result = await showDialog1.ShowAsync();
                    MessageDialog showDialog = new MessageDialog(ex.Message);
                    result = await showDialog.ShowAsync();
                    return;
                }
                l++;
            }
            if (serialPort!=null) // put end command
            {
                byte cmd = 69; // 69 E as end
                await Call_write(cmd, 0);
            }
            Enable_buttons();
        }

        private async Task Call_write(byte cmd, byte to_arduino)
        {
            while (arduinoInput == "K")
            {
                await Task.Run(new Action(() =>
                {
                    Stopwatch _sw = new System.Diagnostics.Stopwatch();
                    _sw.Start();
                    while ((_sw.Elapsed).TotalMilliseconds < 1) { }
                    _sw.Reset();
                }));
            }
            // write
            await WriteAsync_Own(cmd, to_arduino);
            // wait a moment
            await Task.Run(new Action(() =>
            {
                Stopwatch _sw = new System.Diagnostics.Stopwatch();
                _sw.Start();
                while ((_sw.Elapsed).TotalMilliseconds < 2) { }
                _sw.Reset();
            }));
        }

        private void Settings_AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            closeDevice_Click(sender, e);
            this.Frame.Navigate(typeof(Settings), null);
        }

        private void Enable_buttons()
        {
            move.IsEnabled = true;
            cut.IsEnabled = true;
        }

        private void Disable_buttons()
        {
            move.IsEnabled = false;
            cut.IsEnabled = false;
        }
        private void Load_settings()
        {
            CultureInfo customCulture = new CultureInfo("fi-FI");
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            Globals.x_spr = double.Parse(get_setting("x_spr", "200"), customCulture);
            Globals.y_spr = double.Parse(get_setting("y_spr", "200"), customCulture);
            Globals.x_pitch = double.Parse(get_setting("x_pitch", "3.0"), customCulture);
            Globals.y_pitch = double.Parse(get_setting("y_pitch", "3.0"), customCulture);
            Globals.speed = double.Parse(get_setting("move_speed", "200"), customCulture);
            Globals.cutspeed = double.Parse(get_setting("cut_speed", "100"), customCulture);
        }
        private string get_setting(string setting, string default_value)
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            try
            {
                string tst = localSettings.Values[setting].ToString();
            }
            catch (Exception)
            {
                localSettings.Values[setting] = default_value;
                return default_value;
            }

            if (!localSettings.Values[setting].ToString().Equals(null) & !localSettings.Values[setting].ToString().Equals(""))
            {
                return localSettings.Values[setting].ToString();
            }
            else
            {
                localSettings.Values[setting] = default_value;
                return default_value;
            }
        }

        private bool get_bool_setting(string setting, bool default_value)
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            try
            {
                bool tst = (bool)localSettings.Values[setting];
            }
            catch (Exception e)
            {
                localSettings.Values[setting] = default_value;
                return default_value;
            }

            if (!localSettings.Values[setting].ToString().Equals(null) & !localSettings.Values[setting].ToString().Equals(""))
            {
                return (bool)localSettings.Values[setting];
            }
            else
            {
                localSettings.Values[setting] = default_value;
                return default_value;
            }
        }

        private double number_value(string text_string)
        {
            CultureInfo customCulture = new CultureInfo("fi-FI");
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            try
            {
                double test_value = double.Parse(text_string, customCulture.NumberFormat);
                return test_value;
            }
            catch
            {
                return -9999;
            }
        }

        private async void move_Click(object sender, RoutedEventArgs e)
        {
            double step_delay = Globals.x_pitch * 60 * 1000000 / (Globals.speed * Globals.x_spr);
            await Set_speed(step_delay);

            int l_v = 0, l_h = 0, r_v = 0, r_h = 0;
            if (l_up.IsChecked == true) l_v = 116;
            if (l_dn.IsChecked == true) l_v = 32;
            if (l_fw.IsChecked == true) l_h = 192;
            if (l_bk.IsChecked == true) l_h = 128;
            if (r_up.IsChecked == true) r_v = 3;
            if (r_dn.IsChecked == true) r_v = 2;
            if (r_fw.IsChecked == true) r_h = 12;
            if (r_bk.IsChecked == true) r_h = 8;

            int seq = l_v + l_h + r_v + r_h;
            Print_out(77, seq, true);
        }

        private async void cut_Click(object sender, RoutedEventArgs e)
        {
            double step_delay = Globals.x_pitch * 60 * 1000000 / (Globals.cutspeed * Globals.x_spr);
            await Set_speed(step_delay);

            int l_v = 0, l_h = 0, r_v = 0, r_h = 0;
            if (l_up.IsChecked == true) l_v = 116;
            if (l_dn.IsChecked == true) l_v = 32;
            if (l_fw.IsChecked == true) l_h = 192;
            if (l_bk.IsChecked == true) l_h = 128;
            if (r_up.IsChecked == true) r_v = 3;
            if (r_dn.IsChecked == true) r_v = 2;
            if (r_fw.IsChecked == true) r_h = 12;
            if (r_bk.IsChecked == true) r_h = 8;

            int seq = l_v + l_h + r_v + r_h;
            Print_out(77, seq, true);
        }

        private void movement_value_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            int movement = (int)(number_value(movement_value.Text.ToString()) + 0.5);
            int v_movement = (int)(movement * Globals.y_pitch / Globals.x_pitch + 0.5);
            vertical_movement.Text = v_movement.ToString();
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            closeDevice_Click(sender, e);
            this.Frame.Navigate(typeof(info), null);
        }

        private async Task Set_speed(double step_delay_double)
        {
            // convert delay microseconds < 255
            int step_delay = (int)step_delay_double / 100;
            if (step_delay > 255) step_delay = 255;
            await Call_write(83, (byte)step_delay); // 83 S as speed
        }

    }
}
